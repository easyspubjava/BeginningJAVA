# 15. 반복문 

## 1. while문

### 조건이 참(true)인 동안 반복수행하기 

- 주어진 조건에 맞는 동안(true) 지정된 수행문을 반복적으로 수행하는 제어문

- 조건이 맞지 않으면 반복하던 수행을 멈추게 됨

- 조건은 주로 반복 횟수나 값의 비교의 결과에 따라 true, false 판단 됨

- 예) 달리는 자동차, 일정 횟수 만큼 돌아가는 나사못, 특정 온도까지 가동되는 에어컨등


### while문
 
- 수행문을 수행하기 전 조건을 체크하고 그 조건의 결과가 true인 동안 반복 수행

![while](./img/while.png)


### while 문 예제

- 1부터 10까지 더하여 그 결과를 출력해 보자


```

public class WhileTest {

	public static void main(String[] args) {

		int num = 1;
		int sum  = 0;
		
		while( num <= 10) {
			
			sum += num;
			num++;
		}
		
		System.out.println(sum);
		System.out.println(num);
	}

}
```
### 무한 반복 할 때

``` 
   while(true){

       .......
   }
```

## 2. do-while문

### 조건과 상관 없이 한번은 수행문을 수행

- while문은 조건을 먼저 체크하고 반복 수행이 된다면, do-while은 조건과 상관 없이 수행을 한 번 하고나서 조건을 체크

![dowhile](./img/dowhile.png)

- 조건이 맞지 않으면(true 가 아니면) 더 이상 수행하지 않음


### do-while 예제

- 입력받는 모든 숫자의 합을 구하는 예제 단, 입력이 0이 되면 반복을 그만하고 합을 출력

```

import java.util.Scanner;

public class DowhileTest {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int input; 
		int sum = 0;

		do {
			input = scanner.nextInt();
			sum += input;
			
		}while(input != 0);

			
		System.out.println(sum);
	}

}

```
## 3. for문

### for문의 수행 순서

![for](./img/for.png)


### for문 예제 (while과 비교)

- 1부터 10까지 더한 결과를 출력하세요
```
public class ForTest {

	public static void main(String[] args) {
	
		int count =1;
		int sum = 0;
		
		for( int i = 0 ; i<10; i++, count++) {  //10번
			sum += count;
			//count++;
		}
		System.out.println(sum);
		
		
		int num = 1;
		int total = 0;
		
		while( num <= 10) {
			total += num;
			num++;
		}
		System.out.println(total);
	}
}

```

### 각 반복문은 주로 언제 사용하나요?

![loop](./img/loop.png)


### for문의 문장들은 생략가능 합니다.

- 초기화식 생략 : 이미 이전에 값이 초기화 되어 for 내부에서 값을 지정할 필요가 없는 경우

   ![for1](./img/for1.png)

- 조건식 생략 : 반복 수행에 대한 조건이 수행문 내부에 있는 경우
   
   ![for2](./img/for2.png)

- 증감식 생략 : 증감식에 대한 연산이 복잡하거나 다른 변수의 연산 결과값에 결정되는 경우
   
   ![for3](./img/for3.png)

- 무한 반복

   ![for4](./img/for4.png)


### 참고 하세요

    i+1 과 i++은 다릅니다.
    i+1 자체는 i 값이 증가되지 않습니다. 증가하기 위해서는 대입연산자를 써야합니다.
    하지만 i++은 i = i+1, i+=1 과 동일한 의미입니다.
    따라서 값을 1씩 증가하려고 한다면 i++을 사용하세요


